import React, { Component } from "react";
import userList from "../../data/UserList";
import {
  Button,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow
} from "@material-ui/core";
import VisibilityIcon from '@material-ui/icons/Visibility';
import ActivityModal from "../../component/ActivityModal/ActivityModal";

//=========== Table Heading Column
const columns = [
  { id: "id", label: "Id", width: 150, minWidth: 150 },
  { id: "userName", label: "Name", width: 300, minWidth: 300 },
  { id: "timeZone", label: "Time / Zone", width: 150, minWidth: 150 },
  { id: "action", label: "Action", minWidth: 50, width: 50 }
];

class UserList extends Component {
  state = {
    page: 0,
    rowsPerPage: 8,
    activityModal: false,

    userData : "",
  };

  //=================================== Handler ChangeFor Paginatation  ===================================
  handleChangePage = (event, newVaue) => {
    this.setState({
      page: newVaue
    });
  };

  //=================================== Handler ChangeFor Set Row ===================================
  handleChangeRowsPerPage = e => {
    this.setState({
      rowsPerPage: e.target.value
    });
  };

  //==================================== Closing Modal ===================================
  toggleModal = (userData) => {
    this.setState({
      userData,
      activityModal: !this.state.activityModal
    })
  }


  render() {
    // console.log(userList);
    const { page, rowsPerPage } = this.state;
    const rows = [];
    return (
      <div className="user-list">
        <Grid container>
          <TableContainer>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {
                    columns.map((column, index) => (
                      <TableCell
                        key={index}
                        align={column.align}
                        style={{
                          width: column.width,
                          minWidth: column.minWidth,
                          fontWeight: "bold"
                        }}
                      >
                        {column.label}
                      </TableCell>
                    ))
                  }
                </TableRow>
              </TableHead>
              <TableBody>
                {
                  userList.members
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    return (
                      <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                        <TableCell className="py-5">{row.id}</TableCell>
                        <TableCell className="py-5">{row.real_name}</TableCell>
                        <TableCell className="py-5">{row.tz}</TableCell>
                        <TableCell className="py-5">
                            <Button onClick = {() => this.toggleModal(row)}>
                              <VisibilityIcon />
                            </Button>
                        </TableCell>
                      </TableRow>
                    );
                  })
                }
              </TableBody>
            </Table>
          </TableContainer>
          { 
            userList.members.length > 8 ? 
              <TablePagination
                rowsPerPageOptions={[8, 15, 25]}
                component="div"
                count={rows.length}
                rowsPerPage={this.state.rowsPerPage}
                page={this.state.page}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
              : 
              null
          }
        </Grid>

        {
          this.state.activityModal ? 
            <ActivityModal 
              open={this.state.activityModal}
              userInfo={this.state.userData}
              handleClose={this.toggleModal}
            />
          :
            null
        }
      </div>
    );
  }
}

export default UserList;
